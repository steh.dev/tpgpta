console.log("Hello World");
//alert("Passou aqui - chamou a função a gravar");

let nome = "Stephany";
let idade = 20;
let isAluno = true;
let salario = 1234.56; //Números possuem separação por pontos, não virgulas

console.log(typeof(nome));
console.log(typeof(idade));
console.log(typeof(isAluno));

//concatenação
console.log("20 " + nome); 

console.log("A soma de 2 + 2 é " + (2+2));

console.log(`A soma de 2 + 2 é ${2+2}`);

